﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Threading;
using CTREPORTES;
using MySql.Data.MySqlClient;

namespace SAPD
{
    public partial class FrmDiseños : Form
    {
        string SQL_TX;

        public FrmDiseños()
        {
            InitializeComponent();
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            this.Dispose();
            this.DestroyHandle();
            this.Close();
            //Application.Exit();
        }

        private void FrmDiseños_Load(object sender, EventArgs e)
        {
            // cmbtDiseños
            //CARGAR GRILLA CON INSUMOS CREADOS
            CGrilla_Insumos();
            CGCombo_Recetas();
            CGrilla_Insu();

        }

        public void CGCombo_Recetas()
        {
            CmbDiseños.Items.Clear();
            CmbDiseños.SelectedIndex = -1;

            try
            {
                SQL_TX = "SELECT r.REC_NOMBRE FROM RECETASREG r  WHERE r.REC_ESTADO = 1 ";

                ConectDB Cxver = new ConectDB();

                if (Cxver.OpenConnection() == true)
                {

                    MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    //Read the data and store them in the list
                    while (dataReader.Read())
                    {
                        GridInsumos.Rows.Add((dataReader["REC_NOMBRE"].ToString()));
                    }

                    Cxver.CloseConnection();

                }
                /**
                ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();

                while (Cxver.dtr.Read())
                {
                    CmbDiseños.Items.Add((Cxver.dtr["REC_NOMBRE"].ToString()));
                }
                Cxver.dtr.Close();**/
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString());
            }
        }

        public void CGrilla_Insumos()
        {
            try
            {
                SQL_TX = "SELECT i.INSUMO_ID,i.TIP_ID,i.INSU_NOMBRE,i.INSU_UNIDAD,i.INSU_ESTADO,i.POSINSU,t.TIP_AGR FROM INSUMOSREG i, TIPO_INSUMO t WHERE i.INSU_ESTADO = 1 " +
                          " AND (i.TIP_ID = t.TIP_ID) ORDER BY i.TIP_ID,i.INSUMO_ID ";

                ConectDB Cxver = new ConectDB();

                if (Cxver.OpenConnection() == true)
                {

                    MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    //Read the data and store them in the list
                    while (dataReader.Read())
                    {
                       
                        GridInsumos.Rows.Add((dataReader["INSUMO_ID"].ToString()) , (dataReader["INSU_NOMBRE"].ToString()), (dataReader["INSU_UNIDAD"].ToString()), (dataReader["POSINSU"].ToString()));
                    }

                    Cxver.CloseConnection();

                }
                /**ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();

                while (Cxver.dtr.Read())
                {
                    GridInsumos.Rows.Add((Cxver.dtr["INSUMO_ID"].ToString()), (Cxver.dtr["INSU_NOMBRE"].ToString()), "0", (Cxver.dtr["INSU_UNIDAD"].ToString()), (Cxver.dtr["POSINSU"].ToString()));
                }
                Cxver.dtr.Close();**/
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString());
            }
        }

        public void CGrilla_Receta(int IdReceta)
        {
            //double ContGri;
            //int ColPosX, IdInsuX;
            //string ColUnid;
            //int NumREg;
            //GridInsumos.Text = "";
            GridInsumos.Rows.Clear();  

            try
            {
                List<string>[] list = new List<string>[3];

                SQL_TX = "SELECT i.TIP_ID,i.INSU_NOMBRE,r.INSUMO_ID,r.REC_CANTIDAD,r.REC_POS,r.REC_UNIDAD FROM RECETAS r, INSUMOSREG i WHERE r.REC_ID = " + IdReceta + "" +
                         " AND (r.INSUMO_ID = i.INSUMO_ID) ORDER BY i.TIP_ID,i.INSUMO_ID";
                
                ConectDB Cxver = new ConectDB();

                if (Cxver.OpenConnection() == true) {

                    MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    //Read the data and store them in the list
                    while (dataReader.Read())
                    {
                        GridInsumos.Rows.Add((dataReader["INSUMO_ID"].ToString()), (dataReader["INSU_NOMBRE"].ToString()), (dataReader["REC_CANTIDAD"].ToString()), (dataReader["REC_UNIDAD"].ToString()), (dataReader["REC_POS"].ToString()));
                    }

                    Cxver.CloseConnection();
                }

                //ConexDB Cxver = new ConexDB();
                
                //Cxver.cmd = new OleDbCommand(SQL_TX, x);
                //Cxver.dtr = Cxver.cmd.ExecuteReader();

                /**while (Cxver.dtr.Read())
                {
                    GridInsumos.Rows.Add((Cxver.dtr["INSUMO_ID"].ToString()), (Cxver.dtr["INSU_NOMBRE"].ToString()), (Cxver.dtr["REC_CANTIDAD"].ToString()), (Cxver.dtr["REC_UNIDAD"].ToString()), (Cxver.dtr["REC_POS"].ToString()));
                }
                Cxver.dtr.Close();**/
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString());
            }
        }


        private void BtnRegistrar_Click(object sender, EventArgs e)
        {
            DateTime FechaHoy = DateTime.Now;

            if (txtDiseño.Text == "" || txtDiseñoDesc.Text == "")
            {
                MessageBox.Show("Nombre de diseño de concreto sin digitar");
                return;
            }

            if (REvReceta(txtDiseño.Text) == 1)
            {
                MessageBox.Show("Diseño de concreto ya registrado");
                return;
            }
            if (MessageBox.Show("Está seguro de regsitrar el diseño:  [ " + txtDiseño.Text + " ]", " Control registro de Diseño", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            { return; }

            SQL_TX = "INSERT INTO RECETASREG(REC_NOMBRE,REC_DESCRIP,REC_ESTADO,REC_EDAD,REC_RESISTENCIA,REC_FECHA)" +
                     "VALUES('" + txtDiseño.Text + "','" + txtDiseñoDesc.Text + "'," + 1 + ",'" + "-0" + "','" + TxtResistencia.Text + "','" + FechaHoy.ToString("dd/MM/yy") + "')";
            try
            {
                /**ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();**/
                ConectDB Cxver = new ConectDB();
                MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                MySqlDataReader dtr = cmd.ExecuteReader();


                Thread.Sleep(300);

                RegValReceta(txtDiseño.Text);

                CmbDiseños.Items.Clear();
                GridInsumos.Rows.Clear();

                CGrilla_Insumos();
                CGCombo_Recetas();

                dtr.Close();
                cmd.Dispose();

                MessageBox.Show("Diseño de Concreto Registrado");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "No se logro registrar el diseño de concreto");
            }
        }
        public bool RegValReceta(string nameReceta)
        {
            DateTime FechaHoy = DateTime.Now;
            double ContGri;
            int ColPosX;
            int IdInsuX;
            int IdRec;
            string ColUnid;

            IdRec = IDReceta(nameReceta);

            try
            {
                foreach (DataGridViewRow filaX in GridInsumos.Rows)
                {
                    ContGri = Convert.ToDouble(filaX.Cells["ColCantidad"].Value);
                    ColPosX = Convert.ToInt32(filaX.Cells["ColPOS"].Value);
                    ColUnid = Convert.ToString(filaX.Cells["ColUnidad"].Value);
                    IdInsuX = Convert.ToInt32(filaX.Cells["ID_INSU"].Value);

                    SQL_TX = "INSERT INTO RECETAS(REC_ID,INSUMO_ID,REC_ESTADO,REC_CANTIDAD,REC_UNIDAD,REC_POS)" +
                             "VALUES(" + IdRec + "," + IdInsuX + "," + 1 + "," + ContGri + ",'" + ColUnid + "'," + ColPosX + ")";

                    ConexDB Cxver = new ConexDB();
                    Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                    Cxver.dtr = Cxver.cmd.ExecuteReader();
                    Cxver.dtr.Close();

                }

                //MessageBox.Show("Diseño de Concreto Registrado");
                return true;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "No se logro registrar el diseño de concreto");
                return true;
            }

        }
        public int IDReceta(String Receta_nm)
        {
            //int ContaREC = 0;
            int IDRec;

            try
            {
                SQL_TX = "SELECT r.REC_ID, r.REC_NOMBRE FROM RECETASREG r WHERE r.REC_NOMBRE = '" + Receta_nm + "' ";

                ConectDB Cxver = new ConectDB();

                if (Cxver.OpenConnection() == true)
                {

                    MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    //Read the data and store them in the list
                    if (dataReader.Read())
                    {
                        IDRec = Convert.ToInt32(dataReader["REC_ID"].ToString());
                        return IDRec;
                    }
                    else
                    {
                        return 0;
                    } 
                }
                else {
                    return 0;
                }

                Cxver.CloseConnection();

                /**ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();

                if (Cxver.dtr.Read())
                {
                    IDRec = Convert.ToInt32(Cxver.dtr["REC_ID"].ToString());
                    return IDRec;
                }
                else
                {
                    return 0;
                }**/
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString());
                return 0;
            }
        }

        public int REvReceta(String Receta_nm)
        {
            int ContaREC = 0;
            string NmCLi;

            try
            {
                SQL_TX = "SELECT r.REC_NOMBRE FROM RECETASREG r WHERE r.REC_NOMBRE = '" + Receta_nm + "' ";
                ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();

                if (Cxver.dtr.Read())
                {
                    NmCLi = (Cxver.dtr["REC_NOMBRE"].ToString());
                    ContaREC = 1;
                }
                else
                {
                    ContaREC = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString());
            }
            return ContaREC;
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {


            DateTime FechaHoy = DateTime.Now;

            if (CmbDiseños.Text == "")
            {
                MessageBox.Show("No a seleccionado un diseño de concreto");
                return;
            }
            if (MessageBox.Show("Está seguro de modificar el diseño:  [ " + CmbDiseños.Text + " ]", " Control registro de Diseño", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            { return; }

            SQL_TX = "UPDATE RECETASREG SET REC_NOMBRE = '" + txtDiseño.Text + "',REC_DESCRIP='" + txtDiseñoDesc.Text + "',REC_EDAD='" + "-0" + "',REC_RESISTENCIA='" + TxtResistencia.Text + "',REC_FECHA_MOD='" + FechaHoy.ToString("dd/MM/yy") + "' WHERE REC_NOMBRE ='" + CmbDiseños.Text + "' AND REC_ESTADO = 1";

            try
            {
                ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();

                Thread.Sleep(500);

                MOD_Receta(CmbDiseños.Text);

                CmbDiseños.Items.Clear();
                GridInsumos.Rows.Clear();
                CGrilla_Insumos();
                CGCombo_Recetas();

                Cxver.dtr.Close();
                Cxver.cmd.Dispose();
                txtDiseño.Text = "";
                txtDiseñoDesc.Text = "";
                TxtResistencia.Text = "";

                MessageBox.Show("Diseño de Concreto MOdificado OK ");
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "No se logro modificar el diseño de concreto");
            }

        }
        public bool MOD_Receta(string nameReceta)
        {
            
            int IdRec;

            IdRec = IDReceta(nameReceta);
            
            try
            {
                SQL_TX = "DELETE * FROM RECETAS WHERE (REC_ID = " + IdRec + ")";
                ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();                

                RegValReceta(nameReceta);
                Cxver.dtr.Close();

                //    foreach (DataGridViewRow filaX in GridInsumos.Rows)
                //    {
                //        ContGri = Convert.ToDouble(filaX.Cells["ColCantidad"].Value);
                //        ColPosX = Convert.ToInt32(filaX.Cells["ColPOS"].Value);                    
                //        IdInsuX = Convert.ToInt32(filaX.Cells["ID_INSU"].Value);

                //        SQL_TX = "UPDATE RECETAS SET REC_CANTIDAD = " + ContGri + " WHERE REC_ID =" + IdRec + " AND INSUMO_ID =" + IdInsuX + "";
                //        ConexDB Cxver = new ConexDB();
                //        Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                //        Cxver.dtr = Cxver.cmd.ExecuteReader();
                //        Cxver.dtr.Close();
                //    }
                return true;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "No se logro registrar el diseño de concreto");
                return true;
            }
        }
        private void CmbDiseños_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtDiseño.Text = CmbDiseños.Text;
            SQL_TX = "SELECT REC_ID,REC_NOMBRE,REC_DESCRIP,REC_RESISTENCIA FROM RECETASREG WHERE REC_NOMBRE ='" + CmbDiseños.Text + "'";

            //GridInsumos.Rows.Clear();    

            try
            {
                ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();

                if (Cxver.dtr.Read())
                {
                    CGrilla_Receta(Convert.ToInt32(Cxver.dtr["REC_ID"].ToString()));
                    txtDiseñoDesc.Text = Cxver.dtr["REC_DESCRIP"].ToString();
                    TxtResistencia.Text = Cxver.dtr["REC_RESISTENCIA"].ToString();

                    Cxver.cnx.Close();
                    Cxver.dtr.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString());
            }
        }
        public void CGrilla_Insu()
        {
            GridInsu.RowCount = 0;
            try
            {
                SQL_TX = "SELECT i.INSUMO_ID, i.TIP_ID,i.INSU_NOMBRE,i.INSU_UNIDAD,i.POSINSU FROM INSUMOSREG i, TIPO_INSUMO t WHERE i.INSU_ESTADO = 1 " +
                          " AND (i.TIP_ID = t.TIP_ID) ORDER BY i.TIP_ID,i.INSUMO_ID ";
                
                ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();

                while (Cxver.dtr.Read())
                {
                    GridInsu.Rows.Add((Cxver.dtr["INSUMO_ID"].ToString()),(Cxver.dtr["INSU_NOMBRE"].ToString()), "0", (Cxver.dtr["INSU_UNIDAD"].ToString()), (Cxver.dtr["POSINSU"].ToString()));
                }

                Cxver.cnx.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString());
            }
        }

        private void GridInsu_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int celx = 0;
            int Contx = 0;
            string NmInsu;
            
            celx = Convert.ToInt32(e.ColumnIndex.ToString());

            if (GridInsu.Rows.Count == GridInsumos.Rows.Count)
            {
                return; 
            }
            if (e.RowIndex < 0) return;

            if (e.ColumnIndex == celx)
            {
                NmInsu = (string)GridInsu.CurrentRow.Cells[1].Value;

                // CARGA INSUMO FALTANTE
            Contx = 0;

            foreach (DataGridViewRow filaX in GridInsumos.Rows)
            {
                    if (NmInsu != (string)GridInsumos[1, Contx].Value)
                    {                        
                        Contx = Contx + 1;

                        if (Contx >= GridInsumos.Rows.Count)
                        {
                            Contx = Contx + 1;
                        }                        
                    }

                    if (Contx>=GridInsu.Rows.Count-1 )
                    {
                        Contx = (GridInsumos.Rows.Count) - 1;
                        GridInsumos.Rows.Add((string)GridInsu.CurrentRow.Cells[0].Value, NmInsu, "0", (string)GridInsu.CurrentRow.Cells[3].Value, (string)GridInsu.CurrentRow.Cells[4].Value);
                        goto jp1;
                    }
                    
                }

            }
            jp1:;
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {

        }

        private void GridInsumos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
    
}
