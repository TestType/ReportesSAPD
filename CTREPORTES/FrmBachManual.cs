﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace SAPD
{
    public partial class FrmBachManual : Form
    {
        FUNCIONES VerFuc = new FUNCIONES();
        Variables Vrb = new Variables();
        public string sFicINI; //sFicINI = Application.StartupPath + @"\SAPD.ini";
        //private cIniArray mINI = new cIniArray();
        string SQL_TX = "";
        double Vol_mex = 0;
        double NUm_BTH = 0;
        bool RecDiseño = false;
        public FrmBachManual()
        {
            InitializeComponent();

            CG_Recetas();
            CGCombo_Recetas();
            CG_CLientes();
            CG_conductores();
            CG_Mixers();

        }
        private void FrmBachManual_Load(object sender, EventArgs e)
        {
            
        }

        public double HMDAjuste(double ValRalABS, double ValRalHMD, double dblCant)
        {
            double Val1;
            Val1 = dblCant * ((ValRalHMD - ValRalABS) / 100);

            return Val1;
        }

        public double AGR_Ajuste(double ValRalABS, double ValRalHMD, double dblCant)
        {
            double Val1;
            //Val1 = dblCant * (1 + (ValRalHMD - ValRalABS / 100));
            Val1 = dblCant * (1 + (ValRalHMD / 100));
            return Val1;
        }
        public void ChangeHUmAbs()
        {

            double Val_HMD;
            int ID_iNSU;
            int TIP_insu = 0;

            //IdRec = IDReceta(nameReceta);

            try
            {
                foreach (DataGridViewRow filaX in GridTTorden.Rows)
                {
                    TIP_insu = Convert.ToInt32(filaX.Cells["TipID"].Value);
                    if (TIP_insu == 1 || TIP_insu == 2)
                    {
                        Val_HMD = Convert.ToDouble(filaX.Cells["Humedad"].Value);
                        ID_iNSU = Convert.ToInt32(filaX.Cells["ID_INSU"].Value);


                        SQL_TX = "UPDATE INSUMOSREG SET INSU_HUM = " + Val_HMD + " WHERE TIP_ID =" + TIP_insu + " AND INSUMO_ID =" + ID_iNSU + "";

                        ConectDB Cxver = new ConectDB();
                        if (Cxver.OpenConnection() == true)
                        {
                            MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                            MySqlDataReader dtr = cmd.ExecuteReader();
                            dtr.Close();
                        }
                    }
                }
                return;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "No se logro modificar la humedad ");
                return;
            }

        }
        public void CG_Recetas()
        {
            try
            {
                SQL_TX = "SELECT REC_NOMBRE FROM RECETASREG WHERE REC_ESTADO = 1;";

                ConectDB Cxver = new ConectDB();
                if (Cxver.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(this.SQL_TX, Cxver.connection);
                    MySqlDataReader dtr = cmd.ExecuteReader();

                    while (dtr.Read())
                    {
                        cmbRecetas.Items.Add((dtr["REC_NOMBRE"].ToString()));
                    }

                    dtr.Close();
                }
                Cxver.CloseConnection();
                /**
                ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(this.SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();

                while (Cxver.dtr.Read())
                {
                    cmbRecetas.Items.Add((Cxver.dtr["REC_NOMBRE"].ToString()));
                }
                Cxver.dtr.Close();**/
            }
            catch (Exception ex)
            {
                MessageBox.Show("No conexión", ex.ToString());
            }
        }

        public void CGrilla_Receta(int idreceta)
        {
            try
            {
                SQL_TX = "SELECT i.TIP_ID,i.INSU_NOMBRE,r.INSUMO_ID,r.REC_CANTIDAD,r.REC_POS,i.INSU_HUM,i.INSU_ABS,r.REC_UNIDAD FROM RECETAS r, INSUMOSREG i WHERE r.REC_ID = " + idreceta + "" +
                         " AND (r.INSUMO_ID = i.INSUMO_ID) ORDER BY i.TIP_ID,i.INSUMO_ID";

                ConectDB Cxver = new ConectDB();
                if (Cxver.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand();
                    MySqlDataReader dtr = cmd.ExecuteReader();

                    while (dtr.Read())
                    {
                        GridTTorden.Rows.Add((dtr["INSUMO_ID"].ToString()), (dtr["INSU_NOMBRE"].ToString()), (dtr["REC_CANTIDAD"].ToString()), (dtr["INSU_ABS"].ToString()), (dtr["INSU_HUM"].ToString()), "0", "0", (dtr["REC_UNIDAD"].ToString()), (dtr["REC_POS"].ToString()), (dtr["TIP_ID"].ToString()), CG_PIN((dtr["INSU_NOMBRE"].ToString())));
                    }
                    dtr.Close();
                }

                Cxver.CloseConnection();
                /**
                ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();

                while (Cxver.dtr.Read())
                {
                    GridTTorden.Rows.Add((Cxver.dtr["INSUMO_ID"].ToString()), (Cxver.dtr["INSU_NOMBRE"].ToString()), (Cxver.dtr["REC_CANTIDAD"].ToString()), (Cxver.dtr["INSU_ABS"].ToString()), (Cxver.dtr["INSU_HUM"].ToString()), "0", "0", (Cxver.dtr["REC_UNIDAD"].ToString()), (Cxver.dtr["REC_POS"].ToString()), (Cxver.dtr["TIP_ID"].ToString()), CG_PIN((Cxver.dtr["INSU_NOMBRE"].ToString())));
                }
                Cxver.dtr.Close();**/
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString());
            }
        }

        public int CG_PIN(string insuNM)
        {
            int PinSAlida = 0;
            try
            {
                SQL_TX = "SELECT c.CONFIG_PIN FROM CONFIG_SAPD c, INSUMOSREG i  WHERE c.CONFIG_NAME = '" + insuNM + "'";

                ConectDB Cxver = new ConectDB();
                if (Cxver.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                    MySqlDataReader dtr = cmd.ExecuteReader();

                    if (dtr.Read())
                    {
                        PinSAlida = Convert.ToInt32(dtr["CONFIG_PIN"].ToString());
                    }

                    dtr.Close();
                }
                Cxver.CloseConnection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString());
            }
            return PinSAlida;
        }

        public void CGCombo_Recetas()
        {
            cmbRecetas.Items.Clear();
            cmbRecetas.SelectedIndex = -1;

            try
            {
                SQL_TX = "SELECT r.REC_NOMBRE FROM RECETASREG r  WHERE r.REC_ESTADO = 1 ";


                ConectDB Cxver = new ConectDB();
                if (Cxver.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                    MySqlDataReader dtr = cmd.ExecuteReader();

                    while (dtr.Read())
                    {
                        cmbRecetas.Items.Add((dtr["REC_NOMBRE"].ToString()));
                    }
                    dtr.Close();
                }

                Cxver.CloseConnection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString());
            }
        }
        public void CG_CLientes()
        {
            try
            {
                SQL_TX = "SELECT CLI_NOMBRE FROM CLIENTE WHERE CLI_ESTADO = 1;";

                ConectDB Cxver = new ConectDB();
                if (Cxver.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                    MySqlDataReader dtr = cmd.ExecuteReader();

                    while (dtr.Read())
                    {
                        cmbClientes.Items.Add((dtr["CLI_NOMBRE"].ToString()));
                    }
                    dtr.Close();
                }
                Cxver.CloseConnection();
            }
            catch (Exception ex)
            {
                MessageBox.Show("No conexión", ex.ToString());
            }
        }
        public void CGrilla_RecetaCliente(int IdCli)
        {
            cmbRecetas.Text = "";
            cmbRecetas.Items.Clear();
            GridTTorden.Rows.Clear();

            try
            {
                SQL_TX = "SELECT r.REC_NOMBRE,d.REC_ID,d.DET_ESTADO FROM DETALLE_CLI d, RECETASREG r  WHERE d.CLI_ID = " + IdCli + "" +
                         " AND (d.REC_ID = r.REC_ID) ORDER BY r.REC_NOMBRE";

                ConectDB Cxver = new ConectDB();
                if (Cxver.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                    MySqlDataReader dtr = cmd.ExecuteReader();

                    while (dtr.Read())
                    {
                        cmbRecetas.Items.Add(dtr["REC_NOMBRE"].ToString());
                    }
                }
                Cxver.CloseConnection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString());
            }
        }
        public void CGR_Obras(int IDcli)
        {

            cmbObras.Text = "";
            cmbObras.Items.Clear();

            try
            {
                SQL_TX = "SELECT o.OBR_NOMBRE,o.OBR_ESTADO FROM OBRA_CLIENTE o WHERE o.CLI_ID =" + IDcli + " AND o.OBR_ESTADO = 1 " +
                          "ORDER BY o.OBR_NOMBRE ";

                ConectDB Cxver = new ConectDB();
                if (Cxver.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                    MySqlDataReader dtr = cmd.ExecuteReader();

                    while (dtr.Read())
                    {
                        cmbObras.Items.Add((dtr["OBR_NOMBRE"].ToString()));
                    }
                    dtr.Close();
                }

                Cxver.CloseConnection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString());
            }
        }
        public void CG_Mixers()
        {
            try
            {
                SQL_TX = "SELECT MIX_PLACA FROM MIXER WHERE MIX_ESTADO = 1;";

                ConectDB Cxver = new ConectDB();
                if (Cxver.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                    MySqlDataReader dtr = cmd.ExecuteReader();

                    while (dtr.Read())
                    {
                        cmbMixer.Items.Add((dtr["MIX_PLACA"].ToString()));
                    }
                    dtr.Close();
                }
                Cxver.CloseConnection();
            }
            catch (Exception ex)
            {
                MessageBox.Show("No conexión", ex.ToString());
            }
        }
        public void CG_conductores()
        {
            try
            {
                SQL_TX = "SELECT CON_NOMBRE FROM CONDUCTOR WHERE CON_ESTADO = 1 ORDER BY CON_NOMBRE";
                ConectDB Cxver = new ConectDB();
                if (Cxver.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                    MySqlDataReader dtr = cmd.ExecuteReader();

                    while (dtr.Read())
                    {
                        cmbConductor.Items.Add((dtr["CON_NOMBRE"].ToString()));
                    }
                    dtr.Close();
                }
                Cxver.CloseConnection();
            }
            catch (Exception ex)
            {
                MessageBox.Show("No conexión", ex.ToString());
            }
        }

        private void cmbRecetas_SelectedIndexChanged(object sender, EventArgs e)
        {
            SQL_TX = "SELECT REC_ID,REC_NOMBRE,REC_DESCRIP,REC_RESISTENCIA FROM RECETASREG WHERE REC_NOMBRE ='" + cmbRecetas.Text + "'";

            GridTTorden.Rows.Clear();

            try
            {
                ConectDB Cxver = new ConectDB();
                if (Cxver.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                    MySqlDataReader dtr = cmd.ExecuteReader();

                    if (dtr.Read())
                    {
                        CGrilla_Receta(Convert.ToInt32(dtr["REC_ID"].ToString()));
                        LbDEscrip.Text = dtr["REC_DESCRIP"].ToString();

                        
                        dtr.Close();
                    }
                }
                Cxver.CloseConnection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString());
            }
        }

        private void cmbClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            int IdCli = 0;
            cmbObras.Items.Clear();

            try
            {
                SQL_TX = "SELECT CLI_ID FROM CLIENTE WHERE CLI_NOMBRE = '" + cmbClientes.Text + "'";

                ConectDB Cxver = new ConectDB();
                {
                    MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                    MySqlDataReader dtr = cmd.ExecuteReader();

                    if (dtr.Read())
                    {
                        IdCli = Convert.ToInt32(dtr["CLI_ID"].ToString());

                    }

                    CGR_Obras(IdCli);
                    CGrilla_RecetaCliente(IdCli);
                }
                Cxver.CloseConnection();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString());
            }
        }
        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Dispose();
            this.Close();
        }
        private void btnCargar_Click(object sender, EventArgs e)
        {
            string HOraBfIN = "";
            string FechaB = "";
            bool OrdenYasave = false;
            long Idg = 0;
            int IDPlantero = 0;
            int IdObra = 0;
            int IClient = 0;
            int IdMix = 0;
            int IdCoduc = 0;
            int Idreceta = 0;
            double MetrosFin = 0;
            double AprxVol = 0;
            string MiValor = "";
            int ContBach = 0;
            int ORD_NUMBTH = 0;
            string Nu1 = "";
            string Sello = "";

            if (TxVol_Xbaches.Text == ""|| TxVol_Xbaches.Text == "0") { MessageBox.Show("No ha digitado un valor de Volmen (m3) a despachar "); return; }
            if (cmbRecetas.Text == "") { MessageBox.Show("No ha consultado un diseño de concreto "); return; }
            if (cmbMixer.Text == "") { MessageBox.Show("No ha consultado camion mixer "); return; }
            if (cmbConductor.Text == "") { MessageBox.Show("No ha consultado un conductor "); return; }
            if (cmbClientes.Text == "") { MessageBox.Show("No ha consultado cliente "); return; }
            if (Sello==""){ Sello = "0"; }

            // Consultar si ya exite la orden
            if (txtNumOrder.Text == "") { MessageBox.Show("No hay un numero de orden para registrar "); return; } 

            DateTime FechaYa = DateTime.Today;
            FechaB = FechaYa.ToString("MM/dd/yy");
            DateTime HOraYa = DateTime.Now;  //   
            HOraBfIN = HOraYa.ToString("HH:mm:ss");

            ORD_NUMBTH = 1;

            try
            {

                if (GridTTorden.Rows.Count > 1)
                {
                    Idreceta = VerFuc.IdentReceta(cmbRecetas.Text);
                    MetrosFin = Convert.ToInt32(TxVol_Xbaches.Text);
                    IClient = VerFuc.IdentCliente(cmbClientes.Text);
                    IdObra = VerFuc.IdentObra(cmbObras.Text );
                    IdMix = VerFuc.IdentMix(cmbMixer.Text);
                    IdCoduc = VerFuc.IdenConduc(cmbConductor.Text );
                }

                SQL_TX = "INSERT INTO ORDEN_SAVE(ORD_NUM,REC_ID,ORD_VOL,PLANTA_ID,MIXER_ID,COND_ID,OBR_ID, CLI_ID," +
                           "ID_DESPACHA,ORD_FECHA,ORD_HORA,ORD_BTCHIN,ORD_HORAOUT,P_UNIT,ORD_ADJUST,OBS_FRENTE,ORD_NUM_BAH,ORD_SELLO)" +
                            "VALUES('" + txtNumOrder.Text + "'," + Idreceta + "," + MetrosFin + "," + Vrb.PlantaID + "," +
                            "" + IdMix + "," + IdCoduc + "," + IdObra + "," + IClient + "," +
                            "" + Vrb.PlanteroID + ",'" + FechaB + "','" + HOraBfIN + "','" + HOraBfIN + "','" + HOraBfIN + "','-','MN','0'," + ORD_NUMBTH + ",'" + Sello + "')";

                ConectDB Cxver = new ConectDB();
                if (Cxver.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                    MySqlDataReader dtr = cmd.ExecuteReader();


                    dtr.Close();
                    cmd.Dispose();
                }
                

                SaveInsumosOut(txtNumOrder.Text, TxVol_Xbaches.Text, HOraBfIN, HOraBfIN);
                Cxver.CloseConnection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Verificar Salvar Bache");
            }            

        }


        private void btRecalcular_Click(object sender, EventArgs e)
        {
            Vol_mex = Convert.ToDouble(TxVol_Xbaches.Text);
            NUm_BTH = Convert.ToDouble(TxNumBaches.Text);

            if (Vol_mex <= 0 && NUm_BTH <= 0)
            {
                RecDiseño = false;
                return;
            }

            if (cmbRecetas.Text == "")
            {
                MessageBox.Show("Seleccione un diseño para ser recalculado", "Error de Actualización");
                RecDiseño = false;
                return;
            }
            ChangeHUmAbs();

            //   Recalcular recetas implicara remover la receta y hacer los cambios con respecto a la humedad encontrada

            double ValCox = 0;
            int TIP_insu = 0;
            double VAl_regFil;
            int Contx = 0;
            double TTCorreccion = 0;

            try
            {
                foreach (DataGridViewRow filaX in GridTTorden.Rows)
                {
                    TIP_insu = Convert.ToInt32(filaX.Cells["TipID"].Value);
                    
                    if (TIP_insu == 3 || TIP_insu == 4)  //PARA ADITIVOS Y CEMENTANTES
                    {
                        VAl_regFil = Convert.ToDouble(GridTTorden[2, Contx].Value);
                        (GridTTorden[5, Contx].Value) = VAl_regFil * Convert.ToDouble(String.Format("{0:0.000}", TxVol_Xbaches.Text));
                        Contx = Contx + 1;
                    }

                    if (TIP_insu == 1 || TIP_insu == 2)  //PARA AGREGADOS  
                    {
                        VAl_regFil = AGR_Ajuste(Convert.ToDouble(GridTTorden[3, Contx].Value), Convert.ToDouble(GridTTorden[4, Contx].Value), Convert.ToDouble(GridTTorden[2, Contx].Value));
                        (GridTTorden[5, Contx].Value) = Math.Round(VAl_regFil) * Convert.ToDouble(TxVol_Xbaches.Text);
                        TTCorreccion = (HMDAjuste(Convert.ToDouble(GridTTorden[3, Contx].Value), Convert.ToDouble(GridTTorden[4, Contx].Value), Convert.ToDouble(GridTTorden[2, Contx].Value)));
                        ValCox = ValCox + TTCorreccion; // valor correcio para el agua
                        Contx = Contx + 1;
                    }

                    if (TIP_insu == 5) //PARA AGUA
                    {
                        VAl_regFil = Convert.ToDouble(GridTTorden[2, Contx].Value);
                        VAl_regFil = VAl_regFil - ValCox;
                        (GridTTorden[5, Contx].Value) = Math.Round(VAl_regFil) * Convert.ToDouble(TxVol_Xbaches.Text);
                        Contx = Contx + 1;
                    }
                }

                //mINI.IniWrite(Vrb.sFicINI, "Seccion 1", "Vol_XBTH", TxVol_Xbaches.Text); // Resgistrar en Archivo .Ini

                RecDiseño = true; //Verifica que si alla recalculado la orden
                return;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "No se logro la corrección de humedad ");
                RecDiseño = false;
                return;
            }
        }
        private void TxVol_Xbaches_KeyPress(object sender, KeyPressEventArgs e)
        {
            FUNCIONES.SoloNumeros(e);
        }        
        public void SaveInsumosOut(string NmOrden, string VolDesp, string HOraIn, string HOraFin)
        {
            //IdProve
            int Idg = 0;
            int IDProve = 0;
            int NmORD = 0;

            DateTime FechaYa = DateTime.Today;

            SQL_TX = "Select ORD_NUM_BAH From ORDEN_SAVE Where ORD_NUM = '" + NmOrden + "'";

            ConectDB Cxver = new ConectDB();
            if (Cxver.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                MySqlDataReader dtr = cmd.ExecuteReader();



                if (dtr.Read())
                {
                    NmORD = Convert.ToInt32(dtr["ORD_NUM_BAH"].ToString());
                }
                else { NmORD = 0; }

                try
                {
                    for (int ix = 0; ix <= GridTTorden.RowCount - 1; ix++)
                    {
                        //NameIsnu = Convert.ToString(GridTTorden[1, ix].Value);

                        SQL_TX = "INSERT INTO INVENTARIO_OUT (INV_REG,PROV_ID, TIP_ID, INSUMO_ID,INV_TEO,INV_OUT,INV_ABS,INV_HMD," +
                             "INV_UNIT,INV_TIPO,INV_BACHADA,INV_VOL,INV_MA,INV_FECHA,INV_HORA,INV_HORA_INI,INV_MUESTRAS) " +
                             "VALUES('" + txtNumOrder.Text + "'," + 0 + "," + Convert.ToUInt32(GridTTorden[9, ix].Value) + "," + Convert.ToUInt32(GridTTorden[0, ix].Value) + "," +
                                    "" + Convert.ToDouble(GridTTorden[5, ix].Value) + "," + Convert.ToDouble(GridTTorden[6, ix].Value) + "," + Convert.ToDouble(GridTTorden[3, ix].Value) + "," + Convert.ToDouble(GridTTorden[4, ix].Value) + "," +
                                    "'" + (GridTTorden[7, ix].Value).ToString() + "','S'," + NmORD + "," + VolDesp + ",'M'," +
                                    "'" + FechaYa.ToString("MM/dd/yy") + "','" + HOraFin + "','" + HOraIn + "'," + 0 + ")";

                        cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                        dtr = cmd.ExecuteReader();
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "No se logro registra en InventariOUT ");
                    return;
                }
            }
            Cxver.CloseConnection();
        }

    }
}
