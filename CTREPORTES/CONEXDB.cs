﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Windows.Forms;

namespace SAPD
{
    public class ConexDB
    {
        public OleDbConnection cnx;
        public OleDbCommand cmd;
        public OleDbDataReader dtr;
        public OleDbDataAdapter Dtb;

        public string DIrBase = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Application.StartupPath + "\\SAPDATA.accdb;Jet OLEDB:Database Password = SAP0100";

        public ConexDB()
        {
            try
            {
                cnx = new OleDbConnection(DIrBase);
                cnx.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Sin Conexión " + ex.ToString());
            }
        }
    }
}
