﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.IO.Ports;

namespace SAPD
{
    public class FUNCIONES
    {
        string SQL_TX;
        public int Puerto_1;
        public int Puerto_2;
        public int Puerto_3;
        public int Puerto_4;
        public int Puerto_5;

        public static void SoloNumeros(KeyPressEventArgs NumX)
        {
            if (Char.IsDigit(NumX.KeyChar))
            {
                NumX.Handled = false;
            }
            else if (Char.IsSeparator(NumX.KeyChar))
            {
                NumX.Handled = false;
            }
            else if (Char.IsControl(NumX.KeyChar))
            {
                NumX.Handled = false;
            }
            else if (NumX.KeyChar.ToString().Equals("."))
            {
                NumX.Handled = false;
            }
            else
            {
                NumX.Handled = true;
                MessageBox.Show("Solo digitar Numeros y con punto");
            }
        }

        //public static void SoloLetras(KeyPressEventArgs letraX)
        //{
        //    if (Char.IsLetter(letraX.KeyChar))
        //    {
        //        letraX.Handled = false;
        //    }
        //    else if (Char.IsSeparator(letraX.KeyChar))
        //    {
        //        letraX.Handled = false;
        //    }
        //    else if (Char.IsControl(letraX.KeyChar))
        //    {
        //        letraX.Handled = false;
        //    }
        //    else
        //    {
        //        letraX.Handled = true;
        //        MessageBox.Show("Solo Letras");
        //    }
        //}
        public static void IMG_OFF()
        {
            //SAPD Verp = new SAPD();
        }

        public void LimpiaOrdenes()
        {
            //' -->>   Borrar tabla de cola de ordenes

            SQL_TX = "DELETE * FROM GEN_ORDEN";
            ConexDB Cxver = new ConexDB();
            Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
            Cxver.dtr = Cxver.cmd.ExecuteReader();

        }

        public void PortSeriales()
        {
            SQL_TX = "SELECT PLA_LEX1,PLA_LEX2,PLA_LEX3,PLA_LEX4,PLA_LEX5 FROM PLANTA ";
            try
            {
                ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();

                if (Cxver.dtr.Read())
                {
                    Puerto_1 = Convert.ToInt32(Cxver.dtr["PLA_LEX1"].ToString());
                    Puerto_2 = Convert.ToInt32(Cxver.dtr["PLA_LEX2"].ToString());
                    Puerto_3 = Convert.ToInt32(Cxver.dtr["PLA_LEX3"].ToString());
                    Puerto_4 = Convert.ToInt32(Cxver.dtr["PLA_LEX4"].ToString());
                    Puerto_5 = Convert.ToInt32(Cxver.dtr["PLA_LEX5"].ToString());
                }
                Cxver.dtr.Close();
                Cxver.cnx.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("No conexión con TBL Insumos", ex.ToString());
            }
        }
        public void CargaEndOrden()
        {
            Variables CGVari = new Variables();
            SQL_TX = "Select ORD_NUM From ORDEN_SAVE";

            try
            {
                ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();

                if (Cxver.dtr.Read())
                {
                    CGVari.Remision = Convert.ToUInt32(Cxver.dtr["ORD_NUM"].ToString());

                }
                Cxver.dtr.Close();
                Cxver.cnx.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("No conexión con TBL Insumos", ex.ToString());
            }
        }

        public int IdentCliente(string Client)
        {
            SQL_TX = "SELECT CLI_ID FROM CLIENTE WHERE CLI_NOMBRE ='" + Client + "'";

            try
            {
                ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();

                if (Cxver.dtr.Read())
                {
                    return (Convert.ToInt32(Cxver.dtr["CLI_ID"]));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString());
            }
            return 0;
        }

        public int IdentObra(string Obrax)
        {
            try
            {
                SQL_TX = "SELECT o.OBR_ID FROM OBRA_CLIENTE o WHERE o.OBR_NOMBRE = '" + Obrax + "'";

                ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();

                if (Cxver.dtr.Read())
                {
                   return Convert.ToInt32(Cxver.dtr["OBR_ID"].ToString());
                }
                Cxver.dtr.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString());
            }

            return 0;
        }
        public int IdentReceta(string RecetaNm)
        {
            SQL_TX = "SELECT REC_ID FROM RECETASREG WHERE REC_NOMBRE ='" + RecetaNm + "'";
            
            try
            {
                ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();

                if (Cxver.dtr.Read())
                {
                    return (Convert.ToInt32(Cxver.dtr["REC_ID"].ToString()));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString());
            }
            return 0;

        }
        public int IdentMix(string MixNm)
        {
            try
            {
                SQL_TX = "SELECT MIXER_ID FROM MIXER WHERE MIX_PLACA = '" + MixNm + "'";
                ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();

                while (Cxver.dtr.Read())
                {
                    return Convert.ToInt32(Cxver.dtr["MIXER_ID"].ToString());
                }
                Cxver.dtr.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("No conexión con TB Mixer", ex.ToString());
            }
            return 0;

        }
        public int IdenConduc(string CondNm)
        {
            try
            {
                SQL_TX = "SELECT COND_ID FROM CONDUCTOR WHERE CON_NOMBRE = '" + CondNm + "'";
                ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();

                while (Cxver.dtr.Read())
                {
                    return Convert.ToInt32(Cxver.dtr["COND_ID"].ToString());
                }
                Cxver.dtr.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("No conexión con TB Conductores", ex.ToString());
            }

            return 0;
        }

        public int IdProve(String Proveedor_nm)
        {
            try
            {
                SQL_TX = "SELECT p.PROV_ID FROM PROVEEDOR p WHERE p.PRO_EMPRESA = '" + Proveedor_nm + "' ";
                ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();

                if (Cxver.dtr.Read())
                {
                    return Convert.ToInt32(Cxver.dtr["PROV_ID"].ToString());
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString());
            }
            return 0;
        }

    }

}

