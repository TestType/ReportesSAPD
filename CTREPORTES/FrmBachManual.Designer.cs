﻿namespace SAPD
{
    partial class FrmBachManual
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.GridTTorden = new System.Windows.Forms.DataGridView();
            this.label10 = new System.Windows.Forms.Label();
            this.txtNumOrder = new System.Windows.Forms.TextBox();
            this.TxVol_Xbaches = new System.Windows.Forms.TextBox();
            this.TxNumBaches = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbConductor = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbClientes = new System.Windows.Forms.ComboBox();
            this.txtSello = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbMixer = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbObras = new System.Windows.Forms.ComboBox();
            this.btnCargar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.LbDEscrip = new System.Windows.Forms.Label();
            this.btnSalir = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btRecalcular = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbRecetas = new System.Windows.Forms.ComboBox();
            this.ID_INSU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ABSORCION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Humedad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TEO_TOTAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColUnidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPOS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.GridTTorden)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GridTTorden
            // 
            this.GridTTorden.AllowUserToAddRows = false;
            this.GridTTorden.AllowUserToDeleteRows = false;
            this.GridTTorden.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.GridTTorden.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridTTorden.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_INSU,
            this.Column1,
            this.ColCantidad,
            this.ABSORCION,
            this.Humedad,
            this.TEO_TOTAL,
            this.REAL,
            this.ColUnidad,
            this.ColPOS,
            this.TipID,
            this.Column2});
            this.GridTTorden.Location = new System.Drawing.Point(12, 155);
            this.GridTTorden.Name = "GridTTorden";
            this.GridTTorden.RowHeadersVisible = false;
            this.GridTTorden.Size = new System.Drawing.Size(467, 281);
            this.GridTTorden.TabIndex = 33;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(397, 93);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 16);
            this.label10.TabIndex = 37;
            this.label10.Text = "# Batch";
            // 
            // txtNumOrder
            // 
            this.txtNumOrder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNumOrder.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumOrder.ForeColor = System.Drawing.Color.Black;
            this.txtNumOrder.Location = new System.Drawing.Point(577, 49);
            this.txtNumOrder.MaxLength = 4;
            this.txtNumOrder.Name = "txtNumOrder";
            this.txtNumOrder.Size = new System.Drawing.Size(206, 32);
            this.txtNumOrder.TabIndex = 36;
            this.txtNumOrder.Text = "0";
            this.txtNumOrder.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TxVol_Xbaches
            // 
            this.TxVol_Xbaches.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TxVol_Xbaches.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxVol_Xbaches.Location = new System.Drawing.Point(395, 53);
            this.TxVol_Xbaches.MaxLength = 5;
            this.TxVol_Xbaches.Name = "TxVol_Xbaches";
            this.TxVol_Xbaches.Size = new System.Drawing.Size(58, 29);
            this.TxVol_Xbaches.TabIndex = 27;
            this.TxVol_Xbaches.Text = "0";
            this.TxVol_Xbaches.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxVol_Xbaches.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxVol_Xbaches_KeyPress);
            // 
            // TxNumBaches
            // 
            this.TxNumBaches.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TxNumBaches.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxNumBaches.Location = new System.Drawing.Point(395, 112);
            this.TxNumBaches.MaxLength = 4;
            this.TxNumBaches.Name = "TxNumBaches";
            this.TxNumBaches.Size = new System.Drawing.Size(58, 26);
            this.TxNumBaches.TabIndex = 35;
            this.TxNumBaches.Text = "1";
            this.TxNumBaches.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(574, 30);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 16);
            this.label9.TabIndex = 34;
            this.label9.Text = "No.  ORDEN";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cmbConductor);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cmbClientes);
            this.groupBox1.Controls.Add(this.txtSello);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cmbMixer);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cmbObras);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(502, 155);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(339, 281);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Consulta datos registro bache:  ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(9, 136);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 16);
            this.label8.TabIndex = 25;
            this.label8.Text = "Conductor";
            // 
            // cmbConductor
            // 
            this.cmbConductor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmbConductor.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbConductor.FormattingEnabled = true;
            this.cmbConductor.Location = new System.Drawing.Point(9, 155);
            this.cmbConductor.Name = "cmbConductor";
            this.cmbConductor.Size = new System.Drawing.Size(281, 24);
            this.cmbConductor.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 16);
            this.label7.TabIndex = 23;
            this.label7.Text = "Cliente";
            // 
            // cmbClientes
            // 
            this.cmbClientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmbClientes.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbClientes.FormattingEnabled = true;
            this.cmbClientes.Location = new System.Drawing.Point(9, 52);
            this.cmbClientes.Name = "cmbClientes";
            this.cmbClientes.Size = new System.Drawing.Size(324, 24);
            this.cmbClientes.TabIndex = 0;
            this.cmbClientes.SelectedIndexChanged += new System.EventHandler(this.cmbClientes_SelectedIndexChanged);
            // 
            // txtSello
            // 
            this.txtSello.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtSello.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSello.Location = new System.Drawing.Point(175, 207);
            this.txtSello.Name = "txtSello";
            this.txtSello.Size = new System.Drawing.Size(115, 29);
            this.txtSello.TabIndex = 4;
            this.txtSello.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(172, 188);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 16);
            this.label6.TabIndex = 20;
            this.label6.Text = "Sello";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 188);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 16);
            this.label5.TabIndex = 19;
            this.label5.Text = "Mixer";
            // 
            // cmbMixer
            // 
            this.cmbMixer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmbMixer.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMixer.FormattingEnabled = true;
            this.cmbMixer.Location = new System.Drawing.Point(12, 207);
            this.cmbMixer.Name = "cmbMixer";
            this.cmbMixer.Size = new System.Drawing.Size(115, 30);
            this.cmbMixer.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 16);
            this.label3.TabIndex = 17;
            this.label3.Text = "Obra";
            // 
            // cmbObras
            // 
            this.cmbObras.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmbObras.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbObras.FormattingEnabled = true;
            this.cmbObras.Location = new System.Drawing.Point(9, 104);
            this.cmbObras.Name = "cmbObras";
            this.cmbObras.Size = new System.Drawing.Size(324, 24);
            this.cmbObras.TabIndex = 1;
            // 
            // btnCargar
            // 
            this.btnCargar.Location = new System.Drawing.Point(577, 92);
            this.btnCargar.Name = "btnCargar";
            this.btnCargar.Size = new System.Drawing.Size(87, 46);
            this.btnCargar.TabIndex = 29;
            this.btnCargar.Text = "REGISTRAR";
            this.btnCargar.UseVisualStyleBackColor = true;
            this.btnCargar.Click += new System.EventHandler(this.btnCargar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 16);
            this.label4.TabIndex = 31;
            this.label4.Text = "Descripción del diseño";
            // 
            // LbDEscrip
            // 
            this.LbDEscrip.BackColor = System.Drawing.Color.Transparent;
            this.LbDEscrip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LbDEscrip.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbDEscrip.Location = new System.Drawing.Point(12, 117);
            this.LbDEscrip.Name = "LbDEscrip";
            this.LbDEscrip.Size = new System.Drawing.Size(355, 23);
            this.LbDEscrip.TabIndex = 30;
            this.LbDEscrip.Text = "-";
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(697, 92);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(87, 46);
            this.btnSalir.TabIndex = 32;
            this.btnSalir.Text = "SALIR";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 16);
            this.label2.TabIndex = 25;
            this.label2.Text = "Diseños de Concreto";
            // 
            // btRecalcular
            // 
            this.btRecalcular.Location = new System.Drawing.Point(477, 53);
            this.btRecalcular.Name = "btRecalcular";
            this.btRecalcular.Size = new System.Drawing.Size(53, 85);
            this.btRecalcular.TabIndex = 28;
            this.btRecalcular.Text = "RECALCULAR";
            this.btRecalcular.UseVisualStyleBackColor = true;
            this.btRecalcular.Click += new System.EventHandler(this.btRecalcular_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(392, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 16);
            this.label1.TabIndex = 24;
            this.label1.Text = "Vol (m3)";
            // 
            // cmbRecetas
            // 
            this.cmbRecetas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmbRecetas.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbRecetas.FormattingEnabled = true;
            this.cmbRecetas.Location = new System.Drawing.Point(12, 53);
            this.cmbRecetas.Name = "cmbRecetas";
            this.cmbRecetas.Size = new System.Drawing.Size(355, 28);
            this.cmbRecetas.TabIndex = 26;
            this.cmbRecetas.SelectedIndexChanged += new System.EventHandler(this.cmbRecetas_SelectedIndexChanged);
            // 
            // ID_INSU
            // 
            this.ID_INSU.HeaderText = "ID_INSUMO";
            this.ID_INSU.Name = "ID_INSU";
            this.ID_INSU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ID_INSU.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ID_INSU.Visible = false;
            // 
            // Column1
            // 
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.NullValue = null;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column1.DividerWidth = 2;
            this.Column1.HeaderText = "INSUMO";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Width = 160;
            // 
            // ColCantidad
            // 
            this.ColCantidad.HeaderText = "CANTIDAD    (1 m3)";
            this.ColCantidad.Name = "ColCantidad";
            this.ColCantidad.ReadOnly = true;
            this.ColCantidad.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColCantidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColCantidad.Width = 70;
            // 
            // ABSORCION
            // 
            this.ABSORCION.HeaderText = "ABS";
            this.ABSORCION.Name = "ABSORCION";
            this.ABSORCION.ReadOnly = true;
            this.ABSORCION.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ABSORCION.Width = 34;
            // 
            // Humedad
            // 
            this.Humedad.HeaderText = "HMD";
            this.Humedad.Name = "Humedad";
            this.Humedad.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Humedad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Humedad.Width = 34;
            // 
            // TEO_TOTAL
            // 
            this.TEO_TOTAL.HeaderText = "CALCULADO";
            this.TEO_TOTAL.Name = "TEO_TOTAL";
            this.TEO_TOTAL.ReadOnly = true;
            this.TEO_TOTAL.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TEO_TOTAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.TEO_TOTAL.Width = 75;
            // 
            // REAL
            // 
            this.REAL.HeaderText = "REAL";
            this.REAL.Name = "REAL";
            this.REAL.Width = 60;
            // 
            // ColUnidad
            // 
            this.ColUnidad.HeaderText = "Un";
            this.ColUnidad.Name = "ColUnidad";
            this.ColUnidad.ReadOnly = true;
            this.ColUnidad.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColUnidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColUnidad.Width = 30;
            // 
            // ColPOS
            // 
            this.ColPOS.HeaderText = "POS";
            this.ColPOS.Name = "ColPOS";
            this.ColPOS.ReadOnly = true;
            this.ColPOS.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColPOS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColPOS.Visible = false;
            this.ColPOS.Width = 35;
            // 
            // TipID
            // 
            this.TipID.HeaderText = "TipoID";
            this.TipID.Name = "TipID";
            this.TipID.ReadOnly = true;
            this.TipID.Visible = false;
            this.TipID.Width = 50;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "PIN";
            this.Column2.Name = "Column2";
            this.Column2.Visible = false;
            this.Column2.Width = 50;
            // 
            // FrmBachManual
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 450);
            this.Controls.Add(this.GridTTorden);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtNumOrder);
            this.Controls.Add(this.TxVol_Xbaches);
            this.Controls.Add(this.TxNumBaches);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnCargar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.LbDEscrip);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btRecalcular);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbRecetas);
            this.Name = "FrmBachManual";
            this.Text = "FrmBachManual";
            this.Load += new System.EventHandler(this.FrmBachManual_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridTTorden)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView GridTTorden;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txtNumOrder;
        private System.Windows.Forms.TextBox TxVol_Xbaches;
        private System.Windows.Forms.TextBox TxNumBaches;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbConductor;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbClientes;
        private System.Windows.Forms.TextBox txtSello;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbMixer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbObras;
        private System.Windows.Forms.Button btnCargar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LbDEscrip;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btRecalcular;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbRecetas;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_INSU;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn ABSORCION;
        private System.Windows.Forms.DataGridViewTextBoxColumn Humedad;
        private System.Windows.Forms.DataGridViewTextBoxColumn TEO_TOTAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn REAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColUnidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPOS;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
    }
}