﻿namespace SAPD
{
    partial class FrmDiseños
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.GridInsumos = new System.Windows.Forms.DataGridView();
            this.ID_INSU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColUnidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPOS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CmbDiseños = new System.Windows.Forms.ComboBox();
            this.txtDiseñoDesc = new System.Windows.Forms.TextBox();
            this.BtnRegistrar = new System.Windows.Forms.Button();
            this.BtnSalir = new System.Windows.Forms.Button();
            this.TxtResistencia = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnExportar = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDiseño = new System.Windows.Forms.TextBox();
            this.GridInsu = new System.Windows.Forms.DataGridView();
            this.Column0 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.GridInsumos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridInsu)).BeginInit();
            this.SuspendLayout();
            // 
            // GridInsumos
            // 
            this.GridInsumos.AllowUserToAddRows = false;
            this.GridInsumos.AllowUserToDeleteRows = false;
            this.GridInsumos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridInsumos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_INSU,
            this.ColName,
            this.ColCantidad,
            this.ColUnidad,
            this.ColPOS});
            this.GridInsumos.Location = new System.Drawing.Point(8, 173);
            this.GridInsumos.Name = "GridInsumos";
            this.GridInsumos.RowHeadersVisible = false;
            this.GridInsumos.Size = new System.Drawing.Size(471, 278);
            this.GridInsumos.TabIndex = 0;
            this.GridInsumos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridInsumos_CellContentClick);
            // 
            // ID_INSU
            // 
            this.ID_INSU.HeaderText = "ID_INSUMO";
            this.ID_INSU.Name = "ID_INSU";
            // 
            // ColName
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.NullValue = null;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.ColName.DefaultCellStyle = dataGridViewCellStyle1;
            this.ColName.DividerWidth = 2;
            this.ColName.HeaderText = "INSUMO";
            this.ColName.Name = "ColName";
            this.ColName.ReadOnly = true;
            this.ColName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColName.Width = 160;
            // 
            // ColCantidad
            // 
            this.ColCantidad.HeaderText = "CANTIDAD";
            this.ColCantidad.Name = "ColCantidad";
            this.ColCantidad.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // ColUnidad
            // 
            this.ColUnidad.HeaderText = "Un";
            this.ColUnidad.Name = "ColUnidad";
            this.ColUnidad.ReadOnly = true;
            this.ColUnidad.Width = 50;
            // 
            // ColPOS
            // 
            this.ColPOS.HeaderText = "POS";
            this.ColPOS.Name = "ColPOS";
            this.ColPOS.ReadOnly = true;
            this.ColPOS.Width = 50;
            // 
            // CmbDiseños
            // 
            this.CmbDiseños.FormattingEnabled = true;
            this.CmbDiseños.Location = new System.Drawing.Point(13, 133);
            this.CmbDiseños.Name = "CmbDiseños";
            this.CmbDiseños.Size = new System.Drawing.Size(427, 21);
            this.CmbDiseños.TabIndex = 1;
            this.CmbDiseños.SelectedIndexChanged += new System.EventHandler(this.CmbDiseños_SelectedIndexChanged);
            // 
            // txtDiseñoDesc
            // 
            this.txtDiseñoDesc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtDiseñoDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiseñoDesc.Location = new System.Drawing.Point(13, 81);
            this.txtDiseñoDesc.Name = "txtDiseñoDesc";
            this.txtDiseñoDesc.Size = new System.Drawing.Size(326, 21);
            this.txtDiseñoDesc.TabIndex = 2;
            // 
            // BtnRegistrar
            // 
            this.BtnRegistrar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRegistrar.Location = new System.Drawing.Point(520, 74);
            this.BtnRegistrar.Name = "BtnRegistrar";
            this.BtnRegistrar.Size = new System.Drawing.Size(88, 57);
            this.BtnRegistrar.TabIndex = 3;
            this.BtnRegistrar.Text = "Registrar";
            this.BtnRegistrar.UseVisualStyleBackColor = true;
            this.BtnRegistrar.Click += new System.EventHandler(this.BtnRegistrar_Click);
            // 
            // BtnSalir
            // 
            this.BtnSalir.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSalir.Location = new System.Drawing.Point(520, 378);
            this.BtnSalir.Name = "BtnSalir";
            this.BtnSalir.Size = new System.Drawing.Size(88, 55);
            this.BtnSalir.TabIndex = 4;
            this.BtnSalir.Text = "SALIR";
            this.BtnSalir.UseVisualStyleBackColor = true;
            this.BtnSalir.Click += new System.EventHandler(this.BtnSalir_Click);
            // 
            // TxtResistencia
            // 
            this.TxtResistencia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TxtResistencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtResistencia.Location = new System.Drawing.Point(345, 81);
            this.TxtResistencia.Name = "TxtResistencia";
            this.TxtResistencia.Size = new System.Drawing.Size(95, 21);
            this.TxtResistencia.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "Consulta Diseño de Concreto";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 15);
            this.label2.TabIndex = 7;
            this.label2.Text = "Descripción del Diseño";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(344, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 15);
            this.label3.TabIndex = 8;
            this.label3.Text = "Resistencia";
            // 
            // btnExportar
            // 
            this.btnExportar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportar.Location = new System.Drawing.Point(520, 295);
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Size = new System.Drawing.Size(88, 55);
            this.btnExportar.TabIndex = 9;
            this.btnExportar.Text = "Exportar";
            this.btnExportar.UseVisualStyleBackColor = true;
            this.btnExportar.Click += new System.EventHandler(this.btnExportar_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificar.Location = new System.Drawing.Point(520, 212);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(88, 55);
            this.btnModificar.TabIndex = 10;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 16);
            this.label4.TabIndex = 12;
            this.label4.Text = "Nombre del Diseño";
            // 
            // txtDiseño
            // 
            this.txtDiseño.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtDiseño.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiseño.Location = new System.Drawing.Point(13, 27);
            this.txtDiseño.Name = "txtDiseño";
            this.txtDiseño.Size = new System.Drawing.Size(326, 21);
            this.txtDiseño.TabIndex = 11;
            // 
            // GridInsu
            // 
            this.GridInsu.AllowUserToAddRows = false;
            this.GridInsu.AllowUserToDeleteRows = false;
            this.GridInsu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridInsu.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column0,
            this.Column1,
            this.Column3,
            this.Column4,
            this.Column5});
            this.GridInsu.Location = new System.Drawing.Point(8, 173);
            this.GridInsu.Name = "GridInsu";
            this.GridInsu.ReadOnly = true;
            this.GridInsu.RowHeadersVisible = false;
            this.GridInsu.Size = new System.Drawing.Size(158, 278);
            this.GridInsu.TabIndex = 13;
            this.GridInsu.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridInsu_CellClick);
            // 
            // Column0
            // 
            this.Column0.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Column0.HeaderText = "TID_INSUMO";
            this.Column0.MinimumWidth = 4;
            this.Column0.Name = "Column0";
            this.Column0.ReadOnly = true;
            this.Column0.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column0.Visible = false;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column1.HeaderText = "INSUMO";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column1.Width = 150;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "CANTIDAD";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Visible = false;
            this.Column3.Width = 70;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "UNIDAD";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Visible = false;
            this.Column4.Width = 75;
            // 
            // Column5
            // 
            this.Column5.FillWeight = 50F;
            this.Column5.HeaderText = "POS";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column5.Visible = false;
            this.Column5.Width = 55;
            // 
            // FrmDiseños
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 480);
            this.Controls.Add(this.GridInsumos);
            this.Controls.Add(this.GridInsu);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDiseño);
            this.Controls.Add(this.btnModificar);
            this.Controls.Add(this.btnExportar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtResistencia);
            this.Controls.Add(this.BtnSalir);
            this.Controls.Add(this.BtnRegistrar);
            this.Controls.Add(this.txtDiseñoDesc);
            this.Controls.Add(this.CmbDiseños);
            this.Name = "FrmDiseños";
            this.Text = "FrmDiseños";
            this.Load += new System.EventHandler(this.FrmDiseños_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridInsumos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridInsu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView GridInsumos;
        private System.Windows.Forms.ComboBox CmbDiseños;
        private System.Windows.Forms.TextBox txtDiseñoDesc;
        private System.Windows.Forms.Button BtnRegistrar;
        private System.Windows.Forms.Button BtnSalir;
        private System.Windows.Forms.TextBox TxtResistencia;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnExportar;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDiseño;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_INSU;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColUnidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPOS;
        private System.Windows.Forms.DataGridView GridInsu;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column0;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
    }
}