﻿using Microsoft.Reporting.WinForms;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Windows.Forms;

namespace SAPD
{
    public partial class VisorReportes : Form
    {
        private DataSet dataset;
        private ReportDataSource dataSource;
        public string FechaInicial { get; set; }
        public string FechaFinal { get; set; }
        public string Orden { get; set; }
        public string Tiporeporte { get; set; }

        public VisorReportes()
        {
            InitializeComponent();
        }

        private void VisorReportes_Load(object sender, EventArgs e)
        {
            /*Configuracion del reporte*/
            dataset = new DataSet();
            reportViewer1.Reset();
            reportViewer1.ProcessingMode = ProcessingMode.Local;

            switch (Tiporeporte) {
                case "EntradaInsumos":
                    reportViewer1.LocalReport.ReportPath = "Reports/REPORTE_INGRESOS.rdlc";
                    GetEntradaInsumos(ref dataset, FechaInicial, FechaFinal);
                    dataSource = new ReportDataSource("DataSetEntradaInsumos", dataset.Tables[0]);
                    break;
                case "SalidaInsumos":
                    reportViewer1.LocalReport.ReportPath = "Reports/REPORTE_SALIDAS.rdlc";
                    GetSalidaInsumos(ref dataset, FechaInicial, FechaFinal);
                    dataSource = new ReportDataSource("DataSetSalidaInsumos", dataset.Tables[0]);
                    break;
                case "TotalPivotExtendido":
                    reportViewer1.LocalReport.ReportPath = "Reports/REPORTE_TOTAL_PIVOT_EXT.rdlc";
                    GetTotalPivotExtendido(ref dataset, FechaInicial, FechaFinal);
                    dataSource = new ReportDataSource("DataSetTotalPivotExtendido", dataset.Tables[0]);
                    break;
                case "TotalInsumos":
                    reportViewer1.LocalReport.ReportPath = "Reports/REPORTE_TOTAL.rdlc";
                    GetTotalInsumos(ref dataset, FechaInicial, FechaFinal);
                    dataSource = new ReportDataSource("DataSetTotalInsumos", dataset.Tables[0]);
                    break;
                case "Remision":
                    reportViewer1.LocalReport.ReportPath = "Reports/REMISION.rdlc";
                    GetRemision(ref dataset, Orden);
                    dataSource = new ReportDataSource("DataSetRemision", dataset.Tables[0]);
                    break;
            }

            /*adicion del datasource en el reporte*/
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(dataSource);
            this.reportViewer1.RefreshReport();
        }

        private void GetEntradaInsumos(ref DataSet dataset,string fechaini, string fechafin)
        {
            string SQL_TX = "SELECT i.INV_REG 'REGISTRO', "+ 
                "p.PRO_EMPRESA as 'PROVEEDOR', "+ 
                "ti.TIP_AGR, " +
                "ir.INSU_DESC, "+ 
                "i.INV_IN as 'CANTIDAD', " +
                "i.INV_UNIT 'UNIDAD', " +
                "DATE_FORMAT(i.INV_FECHA, '%Y-%m-%d') as 'FECHA', " +
                "TIME(i.INV_HORA) as 'HORA' " +
                "FROM inventario_ins i " +
                "LEFT JOIN tipo_insumo ti ON i.TIP_ID = ti.TIP_ID " +
                "LEFT JOIN insumosreg ir ON i.INSUMO_ID = ir.INSUMO_ID " +
                "LEFT JOIN proveedor p ON i.PROV_ID = p.PROV_ID "+
                "WHERE i.INV_FECHA BETWEEN @FechaInicial and @FechaFinal "+
                "ORDER BY 1";

            ConectDB Cxver = new ConectDB();
            Cxver.OpenConnection();
            MySqlCommand Cmd = new MySqlCommand(SQL_TX, Cxver.connection);

            Cmd.Parameters.Add(new MySqlParameter("FechaInicial",fechaini));
            Cmd.Parameters.Add(new MySqlParameter("FechaFinal", fechafin));

            MySqlDataAdapter adaptador = new MySqlDataAdapter(Cmd);
            adaptador.Fill(dataset, "DataTableEntradaInsumos");
            Cxver.CloseConnection();
        }

        private void GetSalidaInsumos(ref DataSet dataset, string fechaini, string fechafin)
        {
            string SQL_TX = "SELECT * FROM printrep WHERE P_FECHA BETWEEN @FechaInicial and @FechaFinal ORDER BY 1";

            ConectDB Cxver = new ConectDB();
            Cxver.OpenConnection();
            MySqlCommand Cmd = new MySqlCommand(SQL_TX, Cxver.connection);

            Cmd.Parameters.Add(new MySqlParameter("FechaInicial", fechaini));
            Cmd.Parameters.Add(new MySqlParameter("FechaFinal", fechafin));

            MySqlDataAdapter adaptador = new MySqlDataAdapter(Cmd);
            adaptador.Fill(dataset, "DataTableSalidaInsumos");
            Cxver.CloseConnection();
        }

        private void GetTotalInsumos(ref DataSet dataset, string fechaini, string fechafin)
        {
            string SQL_TX = "SELECT P_ABAG1, P_ABAG2,P_ABAG3,P_ABAG4, P_ADJUST, P_CLIENTE, P_CONDUC, " +
                "P_DAG1,P_DAG2,P_DAG3,P_DAG4,P_DAG5,P_DAG6,P_DAG7,P_DAG8,P_DAG9,P_DAG10,P_DAG11,P_DAG12,P_FECHA, " +
                "P_HAG1,P_HAG2,P_HAG3,P_HAG4,P_HORA, P_HORAIN, P_PAG1, P_PAG2,P_PAG3,P_PAG4,P_PAG5,P_PAG6," +
                "P_PAG7,P_PAG8,P_PAG9,P_PAG10,P_PAG11,P_PAG12,P_PLANTA, (P_RAG1 - P_TEAG1) as P_TOT1, " +
                "(P_RAG2 - P_TEAG2) as P_TOT2, (P_RAG3 - P_TEAG3 ) as P_TOT3, (P_RAG4 - P_TEAG4 ) as P_TOT4, " +
                "(P_RAG5 - P_TEAG5 ) as P_TOT5, (P_RAG6 - P_TEAG6 ) as P_TOT6, (P_RAG7 - P_TEAG7 ) as P_TOT7, " +
                "(P_RAG8 - P_TEAG8 ) as P_TOT8, (P_RAG9 - P_TEAG9 ) as P_TOT9, (P_RAG10 - P_TEAG10 ) as P_TOT10, " +
                "(P_RAG11 - P_TEAG11 ) as P_TOT11, (P_RAG12 - P_TEAG12 ) as P_TOT12 , P_RECETA, P_TIPAG1,P_TIPAG2," +
                "P_TIPAG3,P_TIPAG4,P_TIPAG5,P_TIPAG6,P_TIPAG7,P_TIPAG8,P_TIPAG9,P_TIPAG10,P_TIPAG11,P_TIPAG12,P_TXAG1," +
                "P_TXAG2 ,P_TXAG3,P_TXAG4,P_TXAG5,P_TXAG6,P_TXAG7,P_TXAG8,P_TXAG9,P_TXAG10,P_TXAG11,P_TXAG12, " +
                "P_UNIT, P_VOL, P_ORDEN, P_MIXER, P_ORDEN " +
                "FROM printrep WHERE P_FECHA BETWEEN @FechaInicial and @FechaFinal ORDER BY 1";

            ConectDB Cxver = new ConectDB();
            Cxver.OpenConnection();
            MySqlCommand Cmd = new MySqlCommand(SQL_TX, Cxver.connection);

            Cmd.Parameters.Add(new MySqlParameter("FechaInicial", fechaini));
            Cmd.Parameters.Add(new MySqlParameter("FechaFinal", fechafin));

            MySqlDataAdapter adaptador = new MySqlDataAdapter(Cmd);
            adaptador.Fill(dataset, "DataTableTotalInsumos");
            Cxver.CloseConnection();
        }

        private void GetTotalPivotExtendido(ref DataSet dataset, string fechaini, string fechafin)
        {
            string SQL_TX = "SELECT o.ORD_NUM ORDEN, r.REC_NOMBRE MEZCLA, o.ORD_VOL VOLUMEN,  c.CLI_NOMBRE CLIENTE, oc.OBR_NOMBRE OBRA,  " +
                "m.MIX_PLACA MIXER, con.CON_NOMBRE CONDUCTOR, o.ORD_NUM_BAH N_BCH, DATE_FORMAT(i.INV_FECHA, '%Y-%m-%d') FECHA,  " +
                "TIME(i.INV_HORA) HORA, i.INSUMO_ID, ins.INSU_NOMBRE INSUMO, i.INV_OUT, i.INV_TEO " +
                "FROM inventario_out i " +
                "LEFT JOIN orden_save o ON i.INV_REG = o.ORD_NUM " +
                "LEFT JOIN recetasreg r ON r.REC_ID = o.REC_ID " +
                "LEFT JOIN cliente c ON c.CLI_ID = o.CLI_ID " +
                "LEFT JOIN obra_cliente oc ON o.OBR_ID = oc.OBR_ID " +
                "LEFT JOIN mixer m  ON o.MIXER_ID = m.MIXER_ID " +
                "LEFT JOIN conductor con ON o.COND_ID = con.COND_ID " +
                "LEFT JOIN insumosreg ins on ins.INSUMO_ID = i.INSUMO_ID " +
                "WHERE i.INV_FECHA BETWEEN @FechaInicial and @FechaFinal " +
                "ORDER BY TIME(i.INV_HORA)";

            ConectDB Cxver = new ConectDB();
            Cxver.OpenConnection();
            MySqlCommand Cmd = new MySqlCommand(SQL_TX, Cxver.connection);

            Cmd.Parameters.Add(new MySqlParameter("FechaInicial", fechaini));
            Cmd.Parameters.Add(new MySqlParameter("FechaFinal", fechafin));

            MySqlDataAdapter adaptador = new MySqlDataAdapter(Cmd);
            adaptador.Fill(dataset, "DataTableTotalPivotExtendido");
            Cxver.CloseConnection();
        }

        private void GetRemision(ref DataSet dataset, string orden)
        {
            string SQL_TX = "SELECT  o.ORD_NUM ORDEN,  r.REC_NOMBRE RECETA, r.REC_DESCRIP RECETA_DESCRIPCION,  o.ORD_VOL VOLUMEN,  " +
                "c.CLI_NOMBRE CLIENTE,  oc.OBR_NOMBRE OBRA_NOMBRE,  m.MIX_PLACA MIXER,  " +
                "con.CON_NOMBRE CONDUCTOR, DATE_FORMAT(o.ORD_FECHA, '%Y-%m-%d') FECHA,  TIME(o.ORD_HORA) HORA " +
                "FROM orden_save o " +
                "LEFT JOIN recetasreg r ON r.REC_ID = o.REC_ID " +
                "LEFT JOIN cliente c ON c.CLI_ID = o.CLI_ID " +
                "LEFT JOIN obra_cliente oc ON o.OBR_ID = oc.OBR_ID " +
                "LEFT JOIN mixer m  ON o.MIXER_ID = m.MIXER_ID " +
                "LEFT JOIN conductor con ON o.COND_ID = con.COND_ID " +
                "WHERE o.ORD_NUM = @Orden";

            ConectDB Cxver = new ConectDB();
            Cxver.OpenConnection();
            MySqlCommand Cmd = new MySqlCommand(SQL_TX, Cxver.connection);
            Cmd.Parameters.Add(new MySqlParameter("Orden", orden));

            MySqlDataAdapter adaptador = new MySqlDataAdapter(Cmd);
            adaptador.Fill(dataset, "DataTableRemision");
            Cxver.CloseConnection();
        }

    }
}
