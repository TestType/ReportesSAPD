﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace SAPD
{
    public partial class FrmConsultas : Form
    {

        string SQL_TX;

        public FrmConsultas()
        {
            InitializeComponent();
        }


        private void FrmConsultas_Load(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            groupBox4.Visible = false;
            groupBox7.Visible = false;
            CGrillaTotal();
            CGrillaSalidas();
            CGrillaEntrada();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            groupBox4.Visible = false;
            groupBox7.Visible = false;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            groupBox4.Visible = true;
            groupBox7.Visible = false;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            groupBox4.Visible = false;
            groupBox7.Visible = true;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            groupBox4.Visible = true;
            groupBox7.Visible = false;
        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        public void CGrillaTotal()
        {
            try
            {
                SQL_TX = "select ti.TIP_AGR, ir.INSU_NOMBRE, sum(io.INV_REG) as 'REAL', sum(io.INV_TEO) as 'TEORICO' , io.INV_REG - io.INV_TEO as 'DIFERENCIA' , io.INV_UNIT  from inventario_out io LEFT JOIN insumosreg ir ON io.INSUMO_ID = ir.INSUMO_ID LEFT JOIN tipo_insumo ti ON io.TIP_ID = ti.TIP_ID WHERE io.INV_FECHA = CURRENT_DATE() GROUP BY io.TIP_ID , io.INV_FECHA";

                ConectDB Cxver = new ConectDB();
                
                if(Cxver.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    //Read the data and store them in the list
                    while (dataReader.Read())
                    {
                        dataGridView4.Rows.Add((dataReader["TIP_ID"].ToString()) , (dataReader["REAL"].ToString()) , (dataReader["TEORICO"].ToString()) , (dataReader["DIFERENCIA"].ToString()), (dataReader["INV_UNIT"].ToString()));
                    }
                    Cxver.CloseConnection();
                }
            }
            catch (Exception e) {
                Debug.WriteLine("Excepcion GrillaTotal " + e.Message);
            }
        }

        public void CGrillaSalidas()
        {
            try
            {
                SQL_TX = "SELECT o.ORD_NUM as 'NUM_ORD', r.RECETAS as RECETAS, o.ORD_VOL , c.CLI_NOMBRE , oc.OBR_NOMBRE , m.MIX_PLACA , con.CON_NOMBRE as 'CONDUCTOR', o.ORD_FECHA , o.ORD_HORAOUT  FROM inventario_out i LEFT JOIN orden_save o ON  i.INV_BACHADA = o.ORD_NUM_BAH LEFT JOIN recetas r ON r.REC_ID = o.REC_ID LEFT JOIN cliente c ON c.CLI_ID = o.CLI_ID LEFT JOIN obra_cliente oc ON o.OBR_ID = oc.OBR_ID LEFT JOIN mixer m  ON o.MIXER_ID = m.MIXER_ID LEFT JOIN conductor con ON o.COND_ID = con.COND_ID where o.ORD_FECHA = CURRENT_DATE()";

                ConectDB Cxver = new ConectDB();

                if (Cxver.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    //Read the data and store them in the list
                    while (dataReader.Read())
                    {
                        dataGridView1.Rows.Add((dataReader["NUM_ORD"].ToString()) , (dataReader["RECETAS"].ToString()) , (dataReader["ORD_VOL"].ToString()) , (dataReader["CLI_NOMBRE"].ToString()) , (dataReader["OBR_NOMBRE"].ToString()) , (dataReader["MIX_PLACA"].ToString()), (dataReader["CONDUCTOR"].ToString()), (dataReader["ORD_FECHA"].ToString()), (dataReader["ORD_HORAOUT"].ToString()));
                    }
                    Cxver.CloseConnection();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Excepcion GrillaTotal " + e.Message);
            }

        }

        public void CGrillaEntrada() {
            try
            {
                SQL_TX = "SELECT i.INV_REG 'REGISTRO', p.PRO_EMPRESA as 'PROVEEDOR', ti.TIP_AGR as 'TIPO_INSUMO' , ir.INSU_DESC as 'INSUMO', i.INV_IN as 'CANTIDAD' , i.INV_UNIT 'UNIDAD' , i.INV_FECHA as 'FECHA' , i.INV_HORA as 'HORA' FROM inventario_ins i LEFT JOIN tipo_insumo ti ON i.TIP_ID = ti.TIP_ID LEFT JOIN insumosreg ir ON i.INSUMO_ID = ir.INSUMO_ID LEFT JOIN proveedor p ON i.PROV_ID = p.PROV_ID WHERE i.INV_FECHA = CURRENT_DATE()";

                ConectDB Cxver = new ConectDB();

                if (Cxver.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(SQL_TX, Cxver.connection);
                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    //Read the data and store them in the list
                    while (dataReader.Read())
                    {
                        dataGridView2.Rows.Add((dataReader["REGISTRO"].ToString()), (dataReader["PROVEEDOR"].ToString()) , (dataReader["TIPO_INSUMO"].ToString()) , (dataReader["INSUMO"].ToString()), (dataReader["CANTIDAD"].ToString()), (dataReader["UNIDAD"].ToString()), (dataReader["FECHA"].ToString()) , (dataReader["HORA"].ToString()));
                    }
                    Cxver.CloseConnection();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Excepcion GrillaTotal " + e.Message);
            }
        }

        public void GenerarReporteIngreso(object sender, EventArgs e)
        {
            VisorReportes visor = new VisorReportes();
            visor.Tiporeporte = "EntradaInsumos";
            visor.FechaInicial = dateTimePicker4.Value.ToString("yyyy-MM-dd HH:mm:ss");
            visor.FechaFinal = dateTimePicker5.Value.ToString("yyyy-MM-dd HH:mm:ss");
            visor.ShowDialog();
        }

        public void GenerarReporteSalida(object sender, EventArgs e)
        {
            VisorReportes visor = new VisorReportes();
            visor.Tiporeporte = "SalidaInsumos";
            visor.FechaInicial = dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss");
            visor.FechaFinal = dateTimePicker2.Value.ToString("yyyy-MM-dd HH:mm:ss");
            visor.ShowDialog();
        }

        public void GenerarReporteTotal(object sender, EventArgs e)
        {
            VisorReportes visor = new VisorReportes();
            visor.Tiporeporte = "TotalPivotExtendido";
            visor.FechaInicial = dateTimePicker8.Value.ToString("yyyy-MM-dd HH:mm:ss");
            visor.FechaFinal = dateTimePicker7.Value.ToString("yyyy-MM-dd HH:mm:ss");
            visor.ShowDialog();
        }


        private void dataGridView4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView2_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void groupBox7_Enter(object sender, EventArgs e)
        {

        }
    }
}
