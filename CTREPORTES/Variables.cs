﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
//using Sharp7;
using System.Diagnostics;

namespace SAPD
{
    
    public class Variables
    {
        string SQL_TX;
        //  __ Llamr nombres de isnumos de tabal InsumosREG               
        //private cIniArray myINI = new cIniArray();
        public string sFicINI = Application.StartupPath + @"\SAPD.ini";

        public long Remision = 0;

        public string DIR_IP_PLC;
        public int[] pineX = new int[34];
        public int[] PosX = new int[20];
        public string[] NameInsumos = new string[30];

        public int[] PinAGRx = new int[5];
        public int[] PosAGRx = new int[5];
        public string[] NameAGR = new string[5];
        public bool BatchNo = false;
        public bool PauseBatch = false;
        public bool STOPBatch = false;
        public string HoraBath = "";
        public int PlanteroID = 0;

        public int PlantaID = 0;

        public bool StopAgua = false;
        public bool StopAD1 = false;
        public bool StopAD2 = false;
        public bool StopAD3 = false;
        public bool StopAD4 = false;
        public bool StopAD5 = false;


        public double PesoBasculaC;
        public double PesoBasculaTolva1;
        public double PesoBasculaTolva2;
        public double PesoBasculaAgua;
        public double PesoBasculaADTV;

        public double TaraBascAG1;
        public double TaraBascAG2;
        public double TaraBascAG3;
        public double TaraBascAgua;
        public double TaraBascCMT;
        public double TaraBascADTV;

        public double PesoVueloAG1;
        public double PesoVueloAG2;
        public double PesoVueloAG3;
        public double PesoVueloAG4;

        public double PesoCastaneoAG1;
        public double PesoCastaneoAG2;
        public double PesoCastaneoAG3;
        public double PesoCastaneoAG4;

        public double PesoVueloCemento1;
        public double PesoVueloCemento2;
        public double PesoVueloCemento3;
        public double PesoVueloCemento4;

        public double RCemento1;
        public double RCemento2;
        public double RCemento3; 

        public double RAgregado1;
        public double RAgregado2;
        public double RAgregado3;
        public double RAgregado4;

        public double RAditivo1;
        public double RAditivo2;
        public double RAditivo3;
        public double RAditivo4;
        public double RAditivo5;

        public double RAgua;

        public double PesoVueloAGua;
        public double ContXLt;
        public double ContXLt1;
        public double ContXLt2;
        public double ContXLt3;
        public double ContXLt4;
        public double ContXLt5;


        public double ValCalibraAGUA;

        public bool AxCMT1 = false;
        public bool AxCMT2 = false;
        public bool AxCMT3 = false;
        public bool AxCMT4 = false;

        public bool AxAG1 = false;
        public bool AxAG2 = false;
        public bool AxAG3 = false;
        public bool AxAG4 = false; 

        public bool AxADT1 = false;
        public bool AxADT2 = false;
        public bool AxADT3 = false;
        public bool AxADT4 = false;
        public bool AxADT5 = false;

        public bool AxAGUA = false;
        public bool AxBanda = false;
        public bool AxCMTO = false;

        public bool AGr1Ready;
        public bool AGr2Ready;
        public bool AGr3Ready;
        public bool AGr4Ready;

        public bool AguRead = false;
        public bool Adtv1Read = false;
        public bool Adtv2Read = false;
        public bool Adtv3Read = false;
        public bool Adtv4Read = false;
        public bool Adtv5Read = false;

        public bool AG1_OK = false;
        public bool AG2_OK = false;
        public bool AG3_OK = false;
        public bool AG4_OK = false;

        public bool Cemento1Ready;
        public bool Cemento2Ready;
        public bool Cemento3Ready;
        public bool Cemento4Ready;

        public bool ADTV1Ready;
        public bool ADTV2Ready;
        public bool ADTV3Ready;
        public bool ADTV4Ready;
        public bool ADTV5Ready;

        public bool AGUAReady;

        public double ContAgua;
        public double ContAgua2;
        public double ContAgua3;
        public double ContAdtv1;
        public double ContAdtv2;
        public double ContAdtv3;
        public double ContAdtv4;
        public double ContAdtv5;

        public bool OKtim1 = false;
        public bool OKtim2 = false;
        public bool OKtim3 = false;
        public bool OKtim4 = false;
        public bool OKtim5 = false;
        public bool OKtim6 = false;
        public bool OKtim7 = false;
        public bool OKtim8 = false;
        public bool OKtim9 = false;

        public bool BatchReady = false;
        public bool FullBAsc_CMTO = false;

        public string TEmp1 = "0";
        public string TEmp2 = "0";
        public string TEmp3 = "0";
        public string TEmp4 = "0";
        public string TEmp5 = "0";
        public string TEmp6 = "0";
        public string TEmp7 = "0";
        public string TEmp8 = "0";
        public string TEmp9 = "0";
        public string TEmp10 = "0";
        public string TEmp11 = "0";
        public string TEmp12 = "0";
        public string TEmp13 = "0";
        public string TEmpAG1 = "0";
        public string TEmpAG2 = "0";
        public string TEmpAG3 = "0";
        public string TEmpAG4 = "0";
        public string TEmpAGua = "0";
        public string TEmpCMTO = "0";        

        public bool TempAG1 = false;
        public bool TempAG2 = false;
        public bool TempAG3 = false;
        public bool TempAG4 = false;

        public bool TmpAG1 = false;
        public bool TmpAG2 = false;
        public bool TmpAG3 = false;
        public bool TmpAG4 = false;

        public string TEmp1CMT = "0";
        public string TEmp2CMT = "0";
        public string TEmp3CMT = "0";
        public string TEmp4CMT = "0";
        public string TEmp5CMT = "0";
        public string TEmp6CMT = "0";
        public bool TEmpDescCMTO = false;

        public bool Nocasta1 = false;
        public bool Nocasta2 = false;
        public bool Nocasta3 = false;

        public double TimCastaAGR1ON = 0;
        public double TimCastaAGR1OFF = 0;
        public double TimCastaAGR2ON = 0;
        public double TimCastaAGR2OFF = 0;
        public double TimCastaAGR3ON = 0;
        public double TimCastaAGR3OFF = 0;
        public double TimCastaAGR4ON = 0;
        public double TimCastaAGR4OFF = 0;

        public double TimVibraAGR1ON = 0;
        public double TimVibraAGR1OFF = 0;
        public double TimVibraAGR2ON = 0;
        public double TimVibraAGR2OFF = 0;

        public double PesoVibraAGR1 = 0;
        public double PesoVibraAGR2 = 0;

        public double PesoFinBascAGR1 = 0;
        public double PesoFinBascAGR2 = 0;

        public double PesoVueloADTV1 = 0;
        public double PesoVueloADTV2 = 0;
        public double PesoVueloADTV3 = 0;
        public double PesoVueloADTV4 = 0;
        public double PesoVueloADTV5 = 0;

        public double ValorPTJEAguaIni = 0;
        public double ValorPTJEAguaMed = 0;
        public double ValorPTJEAguaFin = 0;
        public double ValorAguaAjuste = 0;
        public double TtAguaTemp = 0;
        public int OP_AGUASI=0;
        public int OP_ADTV = 0;
        public double PesoCmtoFinDesc = 0;
        public double TimCmtoFinDesc = 0;
        public double TopeBascCMT = 0;
        public double PesoVibraCMT = 0;
        public double TimVibraCMTON = 0;
        public double TimVibraCMTOFF = 0;
        public double PesoCastaCMT = 0;
        public double TimCastaCMTON =0;
        public double TimCastaCMTOFF =0;

        public int Op_ADJSiloCMTO = 0;
        public int SelectSiloCMTO = 0;
        public double TimDescCMT = 0;

        public double TimDescAGUA = 0;
        public double TimDescBANDA = 0;
        public double TimDescAGR1 = 0;
        public double TimDescAGR2 = 0;
        public double TimDescAGR3 = 0;
        public double TimDescAGR4 = 0;
        public double ValCalibraAgua = 0;
        public double ValCalibraAdtv1 = 0;
        public double ValCalibraAdtv2 = 0;
        public double ValCalibraAdtv3 = 0;
        public double ValCalibraAdtv4 = 0;
        public double ValCalibraAdtv5 = 0;

        public int BORNA_BANDA = 0;
        public int BORNA_VIBRA_AGR1 = 0;
        public int BORNA_VIBRA_AGR2 = 0;
        public int BORNA_VIBRA_CMT = 0;
        public int BORNA_DESCG_CMTO = 0;
        public int BORNA_CPTA_SILO1 = 0;
        public int BORNA_CPTA_SILO2 = 0;
        public int BORNA_CPTA_SILO3 = 0;
        public int BORNA_CPTA_SILO4 = 0;
        public int BORNA_BASC_AGR1 = 0;
        public int BORNA_BASC_AGR2 = 0;

        public int EsTaDTV = 0;
        public double DeltAD1 = 0;
        public double DeltAD2 = 0;
        public double DeltAD3 = 0;
        public double DeltAD4 = 0;
        public double DeltAD5 = 0;

        public string Ordervalue = "0";
        public long NmRemision = 0;
        public bool NoSAveREG = false;

        public Stopwatch TimpAG1 = null;
        public Stopwatch TimpAG2 = null;
        public Stopwatch TimpAG3 = null;
        public Stopwatch TimpAG4 = null;
        public Stopwatch TimpAG5 = null;
        public Stopwatch TimpAG6 = null;
        public Stopwatch TimpAG7 = null;
        public Stopwatch TimpAGua = null;
        public Stopwatch TimpCMTO = null;

        public void CargarVRB()
        {
            //PesoCastaneoAG1 =Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 2", "PesoCAST1", "0"));
            //PesoCastaneoAG2 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 2", "PesoCAST2", "0"));
            //PesoCastaneoAG3 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 2", "PesoCAST3", "0"));
            //PesoCastaneoAG4 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 2", "PesoCAST4", "0"));

            //PesoVueloAG1 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 3", "PesoVueloAG1", "0"));
            //PesoVueloAG2 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 3", "PesoVueloAG2", "0"));
            //PesoVueloAG3 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 3", "PesoVueloAG3", "0"));
            //PesoVueloAG4 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 3", "PesoVueloAG4", "0"));

            //TimCastaAGR1ON = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 4", "TimCastaAGR1ON", "0"));
            //TimCastaAGR1OFF = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 4", "TimCastaAGR1OFF", "0"));
            //TimCastaAGR2ON = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 4", "TimCastaAGR2ON", "0"));
            //TimCastaAGR2OFF = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 4", "TimCastaAGR2OFF", "0"));
            //TimCastaAGR3ON = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 4", "TimCastaAGR3ON", "0"));
            //TimCastaAGR3OFF = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 4", "TimCastaAGR3OFF", "0"));
            //TimCastaAGR4ON = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 4", "TimCastaAGR4ON", "0"));
            //TimCastaAGR4OFF = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 4", "TimCastaAGR4OFF", "0"));

            //TimVibraAGR1ON = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 5", "TimVibraAGR1ON", "0"));
            //TimVibraAGR1OFF = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 5", "TimVibraAGR1OFF", "0"));
            //TimVibraAGR2ON = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 5", "TimVibraAGR2ON", "0"));
            //TimVibraAGR2OFF = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 5", "TimVibraAGR2OFF", "0"));

            //PesoVibraAGR1 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 6", "PesoVibraAGR1", "0"));
            //PesoVibraAGR2 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 6", "PesoVibraAGR2", "0"));

            //PesoFinBascAGR1 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 7", "PesoBascFinAGR1", "0"));
            //PesoFinBascAGR2 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 7", "PesoBascFinAGR2", "0"));

            //PesoVueloAGua = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 8", "PesoVueloAGua", "0"));
            //PesoVueloADTV1 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 8", "PesoVueloADTV1", "0"));
            //PesoVueloADTV2 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 8", "PesoVueloADTV2", "0"));
            //PesoVueloADTV3 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 8", "PesoVueloADTV3", "0"));
            //PesoVueloADTV4 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 8", "PesoVueloADTV4", "0"));
            //PesoVueloADTV5 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 8", "PesoVueloADTV5", "0"));

            //ValorPTJEAguaIni = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 9", "ValorPTJEAguaIni", "0"));
            //ValorPTJEAguaMed = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 9", "ValorPTJEAguaMed", "0"));
            //ValorPTJEAguaFin = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 9", "ValorPTJEAguaFin", "0"));

            //OP_AGUASI = Convert.ToInt32(myINI.IniGet(sFicINI, "Seccion 9", "OPAGUA_SINO", "0"));
            //OP_ADTV = Convert.ToInt32(myINI.IniGet(sFicINI, "Seccion 9", "OPADTV", "0"));

            //PesoVueloCemento1 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 10", "PesoCASTCMT1", "0"));
            //PesoVueloCemento2 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 10", "PesoCASTCMT2", "0"));
            //PesoVueloCemento3 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 10", "PesoCASTCMT3", "0"));

            //PesoCmtoFinDesc = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 10", "PesoFinDEscCMT", "0"));

            //TimCmtoFinDesc = Convert.ToInt32(myINI.IniGet(sFicINI, "Seccion 10", "TimFinDEscCMT", "0"));
            //TopeBascCMT = Convert.ToInt32(myINI.IniGet(sFicINI, "Seccion 10", "TopeBascCMT", "0"));
            //PesoVibraCMT = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 10", "IniVibraCMT", "0"));

            //PesoCastaCMT = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 10", "PesoCastaCMT", "0"));

            //TimCastaCMTON = Convert.ToInt32(myINI.IniGet(sFicINI, "Seccion 10", "TimCastaCMTon", "0"));
            //TimCastaCMTOFF = Convert.ToInt32(myINI.IniGet(sFicINI, "Seccion 10", "TimCastaCMToff", "0"));


            //TimVibraCMTON = Convert.ToInt32(myINI.IniGet(sFicINI, "Seccion 10", "TimVibraCMTON", "0"));
            //TimVibraCMTOFF = Convert.ToInt32(myINI.IniGet(sFicINI, "Seccion 10", "TimVibraCMTOFF", "0"));

            //Op_ADJSiloCMTO = Convert.ToInt32(myINI.IniGet(sFicINI, "Seccion 10", "ADJSiiloCMT", "0"));

            //SelectSiloCMTO = Convert.ToInt32(myINI.IniGet(sFicINI, "Seccion 10", "SelectSiiloCMT", "0"));

            //TimDescCMT = Convert.ToInt32(myINI.IniGet(sFicINI, "Seccion 11", "TimDescCMT", "0"));
            //TimDescAGUA = Convert.ToInt32(myINI.IniGet(sFicINI, "Seccion 11", "TimDescAGUA", "0"));
            //TimDescBANDA = Convert.ToInt32(myINI.IniGet(sFicINI, "Seccion 11", "TimDescBANDA", "0"));
            //TimDescAGR1 = Convert.ToInt32(myINI.IniGet(sFicINI, "Seccion 11", "TimDescAGR1", "0"));
            //TimDescAGR2 = Convert.ToInt32(myINI.IniGet(sFicINI, "Seccion 11", "TimDescAGR2", "0"));
            //TimDescAGR3 = Convert.ToInt32(myINI.IniGet(sFicINI, "Seccion 11", "TimDescAGR3", "0"));
            //TimDescAGR4 = Convert.ToInt32(myINI.IniGet(sFicINI, "Seccion 11", "TimDescAGR4", "0"));

            //ValCalibraAgua = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 12", "ValCalibraAgua", "0"));

            //ValCalibraAdtv1 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 12", "ValCalibraAdtv1", "0"));
            //ValCalibraAdtv2 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 12", "ValCalibraAdtv2", "0"));
            //ValCalibraAdtv3 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 12", "ValCalibraAdtv3", "0"));
            //ValCalibraAdtv4 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 12", "ValCalibraAdtv4", "0"));
            //ValCalibraAdtv5 = Convert.ToDouble(myINI.IniGet(sFicINI, "Seccion 12", "ValCalibraAdtv5", "0"));



            /* */

        }

        public void CARGA_IP()
        {
            //string SQL_TX;                   

            try
            {
                SQL_TX = "SELECT p.PLA_IP_PLC FROM PLANTA p ";
                ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();

                if (Cxver.dtr.Read())
                {                    
                    DIR_IP_PLC = Cxver.dtr["PLA_IP_PLC"].ToString();
                }
                Cxver.dtr.Close();
                Cxver.cnx.Close();                
            }
            catch (Exception ex)
            {
                MessageBox.Show("No conexión con TBL PLANTA", ex.ToString());
            }
        }
        public void CARGA_BornaX()
        {
            
            int RegTabla=0,Inx=1, InxPOs = 1;            

            try
            {
                SQL_TX = "SELECT Count(*) FROM CONFIG_SAPD ";
                ConexDB Cxver = new ConexDB();
                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();                
                
                if (Cxver.dtr.Read())
                {
                    RegTabla =Convert.ToInt32(Cxver.dtr[0].ToString());
                }

                SQL_TX = "SELECT c.CONFIG_TIPO,c.CONFIG_PIN,c.CONFIG_POS, c.CONFIG_NAME FROM CONFIG_SAPD c ORDER BY CONFIG_PIN ";

                Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
                Cxver.dtr = Cxver.cmd.ExecuteReader();

                while (Cxver.dtr.Read())
                {
                    pineX[Inx] = Convert.ToInt32(Cxver.dtr["CONFIG_PIN"].ToString());
                    PosX[InxPOs] = Convert.ToInt32(Cxver.dtr["CONFIG_POS"].ToString());
                    NameInsumos[Inx] = Cxver.dtr["CONFIG_NAME"].ToString();
                    Inx = Inx + 1;
                }

                Cxver.dtr.Close();
                Cxver.cnx.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("No conexión con TBL PLANTA", ex.ToString());
            }
        }

        public void CargaPlanta()
        {
            SQL_TX = "SELECT * FROM PLANTA ";
            ConexDB Cxver = new ConexDB();
            Cxver.cmd = new OleDbCommand(SQL_TX, Cxver.cnx);
            Cxver.dtr = Cxver.cmd.ExecuteReader();

            if (Cxver.dtr.Read())
            {
                PlantaID = Convert.ToInt32(Cxver.dtr[0].ToString());
            }
        }
        public void UserOperador()
        {
            //PASSUSER
            //SQL = "Select DESP_NAME,ID_DESPACHA,DESP_CLAVE,DESP_APELLIDO  From PASSUSER Where DESP_NAME= '" & LTrim(RTrim(UCase(txtUserName.Text))) & "' and DESP_ESTADO = 1"

        }

    }


}
