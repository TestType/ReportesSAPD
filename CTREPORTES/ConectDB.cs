﻿using System;
using MySql.Data.MySqlClient;

namespace SAPD
{
    class ConectDB
    {
        public MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;

        public ConectDB()
        {
            Initialize();
        }

        public void Initialize()
        {
            server = "localhost";
            database = "sapdata";
            uid = "root";
            password = "";
            string connectionString;

            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";SslMode = none";


            connection = new MySqlConnection(connectionString);
        }

        public bool OpenConnection()
        {
            try
            {
                connection.Open();
                Console.WriteLine("Connection ok.");
                return true;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Open connection Error [" + ex.Number + "]: " + ex.Message);
                return false;
            }

        }

        //Close connection
        public bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Close connection Error [" + ex.Number + "]: " + ex.Message);
                return false;
            }

        }
     
    }
}
